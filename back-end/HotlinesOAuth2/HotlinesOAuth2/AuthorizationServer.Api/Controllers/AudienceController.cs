﻿using AuthorizationServer.Api.Entities;
using AuthorizationServer.Api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace AuthorizationServer.Api.Controllers
{
    // add an endpoint in our Authorization server which allow registering new Audiences (Resource servers)
    [RoutePrefix("api/audience")]
    public class AudienceController : ApiController
    {
        // this endpoint can be accessed via HTTP POST to http://localhost:18292/api/audience
        // authorization server is responsible for generating the cliend id and the shared symmetric key
        // this key should not be shared with any other parts except the resource server
        [Route("")]
        public IHttpActionResult Post(AudienceModel audienceModel)
        {
            if (!ModelState.IsValid) {
                return BadRequest(ModelState);
            }

            Audience newAudience = AudiencesStore.AddAudience(audienceModel.Name);

            return Ok<Audience>(newAudience);

        }
    }
}
