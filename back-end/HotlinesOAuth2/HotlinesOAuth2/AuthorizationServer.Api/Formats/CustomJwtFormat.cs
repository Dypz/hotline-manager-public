﻿using AuthorizationServer.Api.Entities;
using Microsoft.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.DataHandler.Encoder;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IdentityModel.Tokens;
using System.Linq;
using System.Web;
using Thinktecture.IdentityModel.Tokens;

namespace AuthorizationServer.Api.Formats
{
    // this class is responsible for generating the JWT access token
    public class CustomJwtFormat : ISecureDataFormat<AuthenticationTicket>
    {
        private const string AudiencePropertyKey = "audience";

        private readonly string _issuer = string.Empty;

        private string STR_CONNECTION = ConfigurationManager.ConnectionStrings["connectionString"].ConnectionString;

        // issuer will correspond to our Authorization server
        public CustomJwtFormat(string issuer)
        {
            _issuer = issuer;
        }

        // the JWT generation is done in this method
        public string Protect(AuthenticationTicket data)
        {
            if (data == null)
            {
                throw new ArgumentNullException("data");
            }
            // read the client id from the AuthenticationTicket
            string audienceId = data.Properties.Dictionary.ContainsKey(AudiencePropertyKey) ? data.Properties.Dictionary[AudiencePropertyKey] : null;

            if (string.IsNullOrWhiteSpace(audienceId)) throw new InvalidOperationException("AuthenticationTicket.Properties does not include audience");

            // if the client id is not null, we get the audience
            Audience audience = AudiencesStore.FindAudience(audienceId);

            // read the symmetric key
            string symmetricKeyAsBase64 = audience.Base64Secret;
  
            // decode this key to array of byte, use to create HMAC256 signing key
            var keyByteArray = TextEncodings.Base64Url.Decode(symmetricKeyAsBase64);

            var signingKey = new HmacSigningCredentials(keyByteArray);

            var issued = data.Properties.IssuedUtc;
            var expires = data.Properties.ExpiresUtc;

            // JWT payload is sign by the signing key
            var token = new JwtSecurityToken(_issuer, audienceId, data.Identity.Claims, issued.Value.UtcDateTime, expires.Value.UtcDateTime, signingKey);

            var handler = new JwtSecurityTokenHandler();

            // serialize the JSON web token
            var jwt = handler.WriteToken(token);

            StoreToken(jwt, Constant.USERNAME);

            return jwt;
        }

        public AuthenticationTicket Unprotect(string protectedText)
        {
            throw new NotImplementedException();
        }

        private void StoreToken(string token, string userMail)
        {
            try
            {
                using (MySqlConnection con = new MySqlConnection(STR_CONNECTION))
                {
                    con.Open();
                    MySqlCommand commandSelect = con.CreateCommand();
                    commandSelect.Connection = con;
                    // Use prepare statements to prevent from SQL Injections
                    // Indeed, it escape some extra characters used in some SQL Injections
                    commandSelect.CommandText = "SELECT jwt_token FROM tokens WHERE user_mail=@user_mail;";
                    commandSelect.Parameters.AddWithValue("@user_mail", userMail);
                    bool hasAlreadyToken = false;
                    using (commandSelect)
                    {
                        using (MySqlDataAdapter sda = new MySqlDataAdapter())
                        {
                            commandSelect.Connection = con;
                            sda.SelectCommand = commandSelect;
                            using (DataTable dt = new DataTable())
                            {
                                sda.Fill(dt);
                                foreach (DataRow row in dt.Rows)
                                {
                                    hasAlreadyToken = true;
                                    break;
                                }

                            }
                        }
                    }
                    if (hasAlreadyToken)
                    {
                        MySqlCommand commandUpdate = con.CreateCommand();
                        commandUpdate.CommandText = "UPDATE tokens SET jwt_token=@jwt_token WHERE user_mail=@user_mail ";
                        commandUpdate.Prepare();
                        commandUpdate.Parameters.AddWithValue("@jwt_token", token);
                        commandUpdate.Parameters.AddWithValue("@user_mail", userMail);
                        commandUpdate.ExecuteNonQuery();
                    }
                    else
                    {
                        MySqlCommand commandInsert = con.CreateCommand();
                        commandInsert.CommandText = "INSERT INTO tokens (jwt_token,user_mail) VALUES (@jwt_token,@user_mail)";
                        commandInsert.Prepare();
                        commandInsert.Parameters.AddWithValue("@jwt_token", token);
                        commandInsert.Parameters.AddWithValue("@user_mail", userMail);
                        commandInsert.ExecuteNonQuery();
                    }
                }
            }
            catch(Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.Message);
            }
        }
            
    }
}