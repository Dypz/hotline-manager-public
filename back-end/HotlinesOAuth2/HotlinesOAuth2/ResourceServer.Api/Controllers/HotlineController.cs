﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web.Http;
using ResourceServer.Api.Models;
using MySql.Data.MySqlClient;
using System.Configuration;

namespace ResourceServer.Api.Controllers
{
    [Authorize]
    [RoutePrefix("api/hotlines")]
    public class HotlineController : ApiController
    {
        private string STR_CONNECTION = ConfigurationManager.ConnectionStrings["connectionString"].ConnectionString;

        // GET: api/hotlines/admin?mail={mail}
        [HttpGet]
        [Route("admin")]
        public IEnumerable<Hotline> GetHotlines(string mail)
        {
            try
            {
                var userController = new UserController();
                // First check if it's an administrator
                if (!userController.GetUserStatus(mail))
                    return null;

                // Then check if the token is associated to the user email
                var headers = Request.Headers.Authorization.ToString();
                if (!IsUserToken(headers.Split(null)[1], mail))
                    return null;

                List<Hotline> result = new List<Hotline>();
                using (MySqlConnection con = new MySqlConnection(STR_CONNECTION))
                {
                    using (MySqlCommand cmd = new MySqlCommand("SELECT * FROM hotlinesaved"))
                    {
                        using (MySqlDataAdapter sda = new MySqlDataAdapter())
                        {
                            cmd.Connection = con;
                            sda.SelectCommand = cmd;
                            using (DataTable dt = new DataTable())
                            {
                                sda.Fill(dt);
                                foreach (DataRow row in dt.Rows)
                                {
                                    result.Add(new Hotline()
                                    {
                                        HotlineID = row["id"].ToString(),
                                        Address = row["address"].ToString(),
                                        Description = row["description"].ToString(),
                                        State = (HotlineStates.States)row["state"],
                                        Date = (DateTime)row["date"],
                                        AuthorMail = row["authorMail"].ToString(),
                                        PhoneNumber = row["phoneNumber"].ToString(),
                                        HotlineResponsibleMail = row["hotlineResponsibleMail"].ToString()
                                    });
                                }

                            }
                        }
                    }
                }
                return result;
            }
            catch(Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.Message);
                return null;
            }
            
        }

        // GET: api/hotlines?mail={mail}
        [HttpGet]
        [Route("")]
        public IEnumerable<Hotline> GetHotline(string mail)
        {
            
            try
            {
                // Check if the token is associated to the user email
                var headers = Request.Headers.Authorization.ToString();
                if (!IsUserToken(headers.Split(null)[1], mail))
                    return null;

                List<Hotline> result = new List<Hotline>();
                using (MySqlConnection con = new MySqlConnection(STR_CONNECTION))
                {
                    MySqlCommand command = con.CreateCommand();
                    command.Connection = con;
                    // Use prepare statements to prevent from SQL Injections
                    // Indeed, it escape some extra characters used in some SQL Injections
                    
                    command.CommandText = "SELECT * FROM hotlinesaved WHERE authorMail=@authorMail";
                    command.Parameters.AddWithValue("@authorMail", mail);
                    using (MySqlCommand cmd = command)
                    {
                        using (MySqlDataAdapter sda = new MySqlDataAdapter())
                        {
                            cmd.Connection = con;
                            sda.SelectCommand = cmd;
                            using (DataTable dt = new DataTable())
                            {
                                sda.Fill(dt);
                                foreach (DataRow row in dt.Rows)
                                {
                                    result.Add(new Hotline()
                                    {
                                        HotlineID = row["id"].ToString(),
                                        Address = row["address"].ToString(),
                                        Description = row["description"].ToString(),
                                        State = (HotlineStates.States)row["state"],
                                        PhoneNumber = row["phoneNumber"].ToString(),
                                        Date = (DateTime)row["date"],
                                        AuthorMail = row["authorMail"].ToString(),
                                        HotlineResponsibleMail = row["hotlineResponsibleMail"].ToString()
                                    });
                                }

                            }
                        }
                    }
                }
                return result;
            }
            catch (Exception)
            {
                return null;
            }
            
        }

        // POST: api/hotlines/create
        [HttpPost]
        [Route("create")]
        public bool CreateHotline(Hotline hotline)
        {
            if (!ModelState.IsValid)
            {
                return false;
            }
            try
            {
                // Check if the token is associated to the user email
                var headers = Request.Headers.Authorization.ToString();
                if (!IsUserToken(headers.Split(null)[1], hotline.AuthorMail))
                    return false;

                // Check the date for the hotline before creating it
                if (!CheckDate(hotline.Date))
                    return false;
                

                using (MySqlConnection con = new MySqlConnection(STR_CONNECTION))
                {
                    con.Open();
                    MySqlCommand commandSelect = con.CreateCommand();
                    commandSelect.Connection = con;
                    // Use prepare statements to prevent from SQL Injections
                    // Indeed, it escape some extra characters used in some SQL Injections
                    commandSelect.CommandText = "SELECT id FROM hotlinesaved WHERE authorMail=@authorMail AND (state=@state1 OR state=@state0);";
                    commandSelect.Parameters.AddWithValue("@authorMail", hotline.AuthorMail);
                    commandSelect.Parameters.AddWithValue("@state1", Convert.ToInt32(1));
                    commandSelect.Parameters.AddWithValue("@state0", Convert.ToInt32(0));
                    using (commandSelect)
                    {
                        using (MySqlDataAdapter sda = new MySqlDataAdapter())
                        {
                            commandSelect.Connection = con;
                            sda.SelectCommand = commandSelect;
                            using (DataTable dt = new DataTable())
                            {
                                sda.Fill(dt);
                                foreach (DataRow row in dt.Rows)
                                {
                                    return false;
                                }

                            }
                        }
                    }
                    MySqlCommand commandInsert = con.CreateCommand();
                    commandInsert.CommandText = "INSERT INTO hotlinesaved (id,date,description,address,state,phoneNumber,authorMail) VALUES (@id,@date,@description,@address,@state,@phoneNumber,@authorMail)";
                    commandInsert.Prepare();
                    commandInsert.Parameters.AddWithValue("@id", hotline.HotlineID);
                    commandInsert.Parameters.AddWithValue("@date", hotline.Date.ToString("yyyy-MM-dd H:mm:ss"));
                    commandInsert.Parameters.AddWithValue("@description", hotline.Description);
                    commandInsert.Parameters.AddWithValue("@address", hotline.Address);
                    commandInsert.Parameters.AddWithValue("@state", Convert.ToInt32(0));
                    commandInsert.Parameters.AddWithValue("@phoneNumber", hotline.PhoneNumber);
                    commandInsert.Parameters.AddWithValue("@authorMail", hotline.AuthorMail);
                    commandInsert.ExecuteNonQuery();
                    return true;
                }
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.Message);
                return false;
            }


        }

        // PUT : api/hotlines/update
        [HttpPut]
        [Route("update")]
        public bool UpdateHotline(Hotline hotline)
        {
            if (!ModelState.IsValid)
            {
                return false;
            }
            try
            {
                // Check if the token is associated to the user email who created the hotline or the connected admin
                var headers = Request.Headers.Authorization.ToString();
                if (!IsUserToken(headers.Split(null)[1], hotline.AuthorMail) && !IsUserToken(headers.Split(null)[1], hotline.HotlineResponsibleMail))
                    return false;

                // Check the date for the hotline before creating it
                if (!CheckDate(hotline.Date))
                    return false;

                using (MySqlConnection con = new MySqlConnection(STR_CONNECTION))
                {
                    con.Open();
                    MySqlCommand command = con.CreateCommand();
                    command.Connection = con;
                    // Use prepare statements to prevent from SQL Injections
                    // Indeed, it escape some extra characters used in some SQL Injections
                    command.CommandText = "UPDATE hotlinesaved SET date=@date , description=@description , address=@address , state=@state , phoneNumber=@phoneNumber , hotlineResponsibleMail=@hotlineResponsibleMail WHERE id=@id ";
                    command.Prepare();
                    command.Parameters.AddWithValue("@id", hotline.HotlineID);
                    command.Parameters.AddWithValue("@date", hotline.Date.ToString("yyyy-MM-dd H:mm:ss"));
                    command.Parameters.AddWithValue("@description", hotline.Description);
                    command.Parameters.AddWithValue("@address", hotline.Address);
                    command.Parameters.AddWithValue("@state", (int)hotline.State);
                    command.Parameters.AddWithValue("@phoneNumber", hotline.PhoneNumber);
                    command.Parameters.AddWithValue("@hotlineResponsibleMail", hotline.HotlineResponsibleMail);
                    command.ExecuteNonQuery();
                    return true;
                }
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.Message);
                return false;
            }
        }

        // DELETE : api/hotlines/delete?id={id}&mail={mail}
        [HttpDelete]
        [Route("delete")]
        public bool RemoveHotline(string id, string mail)
        {
            try
            {
                // Check if the token is associated to the user email
                var headers = Request.Headers.Authorization.ToString();
                if (!IsUserToken(headers.Split(null)[1], mail))
                    return false;

                using (MySqlConnection con = new MySqlConnection(STR_CONNECTION))
                {
                    con.Open();
                    MySqlCommand command = con.CreateCommand();
                    command.Connection = con;
                    command.CommandText = "DELETE FROM hotlinesaved WHERE id=@id AND (authorMail=@authorMail OR hotlineResponsibleMail=@hotlineResponsibleMail)";
                    command.Parameters.AddWithValue("@id", id);
                    command.Parameters.AddWithValue("@authorMail", mail);
                    command.Parameters.AddWithValue("@hotlineResponsibleMail", mail);
                    command.ExecuteNonQuery();
                    return true;
                }
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.Message);
                return false;
            }

        }

        private bool IsUserToken(string token, string userMail)
        {
            try
            {
                using (MySqlConnection con = new MySqlConnection(STR_CONNECTION))
                {
                    con.Open();
                    MySqlCommand commandSelect = con.CreateCommand();
                    commandSelect.Connection = con;
                    // Use prepare statements to prevent from SQL Injections
                    // Indeed, it escape some extra characters used in some SQL Injections
                    commandSelect.CommandText = "SELECT jwt_token FROM tokens WHERE user_mail=@user_mail;";
                    commandSelect.Parameters.AddWithValue("@user_mail", userMail);
                    using (commandSelect)
                    {
                        using (MySqlDataAdapter sda = new MySqlDataAdapter())
                        {
                            commandSelect.Connection = con;
                            sda.SelectCommand = commandSelect;
                            using (DataTable dt = new DataTable())
                            {
                                sda.Fill(dt);
                                foreach (DataRow row in dt.Rows)
                                {
                                    if (row["jwt_token"].ToString() == token)
                                    { return true; }
                                    else { return false; }
                                }

                            }
                        }
                    }
                    return false;
                }
            }
            catch(Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.Message);
                return false;
            }
            
        }

        private bool CheckDate(DateTime date)
        {
            if (date.Year != 2017)
                return false;
            if (date.Month != 3)
                return false;
            if (date.Day < 27 || date.Day > 31)
                return false;
            if (date.Hour < 7 || date.Hour > 21)
                return false;
            return true;
        }

    }
}