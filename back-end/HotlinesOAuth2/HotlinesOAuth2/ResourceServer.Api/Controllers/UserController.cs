﻿using MySql.Data.MySqlClient;
using System.Configuration;
using System.Data;
using System.Web.Http;

namespace ResourceServer.Api.Controllers
{

    [Authorize]
    [RoutePrefix("api/userstatus")]
    public class UserController : ApiController
    {
        private string STR_CONNECTION = ConfigurationManager.ConnectionStrings["connectionString"].ConnectionString;

        // GET : api/userstatus?mail={mail}
        [Route("")]
        [HttpGet]
        public bool GetUserStatus(string mail)
        {
            using (MySqlConnection con = new MySqlConnection(STR_CONNECTION))
            {
                MySqlCommand command = con.CreateCommand();
                command.Connection = con;
                // Use prepare statements to prevent from SQL Injections
                // Indeed, it escape some extra characters used in some SQL Injections
                command.CommandText = "SELECT mail FROM administrators WHERE mail=@mail";
                command.Parameters.AddWithValue("@mail", mail);
                using (command)
                {
                    using (MySqlDataAdapter sda = new MySqlDataAdapter())
                    {
                        command.Connection = con;
                        sda.SelectCommand = command;
                        using (DataTable dt = new DataTable())
                        {
                            sda.Fill(dt);
                            foreach (DataRow row in dt.Rows)
                            {
                                if (row["mail"].ToString().Equals(mail))
                                    return true;
                            }

                        }
                    }
                }
            }
            return false;
        }
    }
}
