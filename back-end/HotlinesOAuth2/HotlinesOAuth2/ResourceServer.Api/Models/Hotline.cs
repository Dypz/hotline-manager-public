﻿using System;
using System.Runtime.Serialization;

namespace ResourceServer.Api.Models
{
    [DataContract]
    public class Hotline
    {
        [DataMember(Name = "Date")]
        public DateTime Date { get; set; }

        [DataMember(Name = "Description")]
        public string Description { get; set; }

        [DataMember(Name = "Address")]
        public string Address { get; set; }

        [DataMember(Name = "State")]
        public HotlineStates.States State { get; set; }

        [DataMember(Name = "PhoneNumber")]
        public string PhoneNumber { get; set; }

        [DataMember(Name = "AuthorMail")]
        public string AuthorMail { get; set; }

        [DataMember(Name = "HotlineResponsibleMail")]
        public string HotlineResponsibleMail { get; set; } = null;

        [DataMember(Name = "HotlineID")]
        public string HotlineID { get; set; }
    }

    public class HotlineStates
    {
        public enum States { InProcessed, Validated, Done }
    }
}