'use strict';

angular.module('hotlineManager')
    .component('hotlineManager',{
        templateUrl : 'hotline-manager/hotline-manager.template.html',
        controller : ['Hotlines','UserStatus','$location','$routeParams', '$rootScope', '$window','$scope','$timeout','ngProgressLite',
            function (Hotlines, UserStatus, $location, $routeParams, $rootScope, $window, $scope, $timeout,ngProgressLite) {

            var self = this;
            // Initialize the displayers
            this.showSuccess = false;
            this.showAlert = false;
            this.successMessage = "";
            this.alertMessage = "";
            this.loginSuccess = false;
            $scope.isAdmin = false;
            $scope.userName = null;

            $scope.maxQuantity = 5;

            this.showMore = function(){
                $scope.maxQuantity += 5;
            };

            this.showLess = function(){
                $scope.maxQuantity -= 5;
            };


            /********   Authentication with ADAL  **************************/

            /*var ADAL = new AuthenticationContext({
                instance: 'https://login.microsoftonline.com/',
                tenant: 'common',

                clientId: '6c7285c5-1f55-4f1e-8600-6141ca45f210',
                redirectUri: 'http://papisen.000webhostapp.com',
                postLogoutRedirectUri: 'http://papisen.000webhostapp.com',

                callback: userSignedIn,
                popUp: true
            });*/
            var ADAL = new AuthenticationContext({
                instance: 'https://login.microsoftonline.com/',
                tenant: 'common',

                clientId: '5b3a9668-01de-4f2b-851b-e1eaff7e8175',
                redirectUri: 'http://localhost:8000',
                postLogoutRedirectUri: 'http://localhost:8000',

                callback: userSignedIn,
                popUp: true
            });



                this.signIn = function() {
                ADAL.login();
                ngProgressLite.start();
            };

            this.logOut = function(){
                ADAL.logOut();
                $rootScope.currentUser = null;
            };

            function userSignedIn(err, token) {
                if (!err) {
                    ADAL.acquireToken(ADAL.config.clientId, validateToken(err,token));;
                }
                else {
                    console.log("error: " + err);
                }
            }

                function validateToken(err, token)
                {
                    if (err)
                    {
                        console.log(err);
                        return;
                    }
                    $rootScope.currentUser = ADAL.getCachedUser();
                    // user name must be defined as the email address for yncrea's account
                    // normally the tenant is yncrea so the user can not use another Microsoft account
                    if($rootScope.currentUser.userName.split('@')[1] !== 'isen.yncrea.fr'){
                        self.loginSuccess = false;
                        return;
                    }
                    UserStatus.isAdministrator($rootScope.currentUser.userName,callbackUserStatus);

                }

            function callbackUserStatus(isAdmin){
                if(isAdmin == null){
                    self.loginSuccess = false;
                    return;
                }
                $rootScope.isAdministrator = isAdmin;
                self.loginSuccess = true;
                // Initialize the list of hotlines
                getHotlinesAPI();

                ngProgressLite.done();

                $scope.isAdmin = isAdmin;
                $scope.userName = $rootScope.currentUser.profile.name;
                $timeout(function(){
                    $scope.$apply();
                });
            }

            /*****************     End authentication with ADAL *********************/


            var checkRoute = function checkRoute(){
                // Back button redirect to /home in certain cases while we check route
                // So we force redirection here if our controller has been loaded
                if($location.path !== '/hotlines' && $routeParams.status === undefined && $routeParams.hotlineId === undefined)
                    $location.path('/hotlines');

                if($routeParams.status !== undefined && $routeParams.hotlineId !== undefined){
                    // First : an error occured
                    if($routeParams.status === 'error'){
                        failedDefinition();
                        return;
                    }
                    // Second : we check if the id correspond to the last hotline removed
                    if($rootScope.hotlineRemoved === $routeParams.hotlineId){
                        if($routeParams.status === 'success'){
                            successDefinition();
                            return;
                        }
                    }
                    // If it's not the last hotline removed
                    // Then we check if the id correspond to one of our current hotlines
                    var success = false;
                    angular.forEach($rootScope.hotlines, function(value,key){
                        if(value.HotlineID === $routeParams.hotlineId){
                            if($routeParams.status === 'success'){
                                success = true;
                                successDefinition();
                                return;
                            }
                        }
                    });
                    if(!success){
                        // In this case, the url does not respect any of the existing URL
                        // So we redirect him
                        $location.path('/hotlines');
                        return;
                    }

                }
            };

            function callbackHotlineRequest(hotlines) {
                if(hotlines == null){
                    errorLoading();
                    checkRoute();
                    isAutorisedToCreate();
                    return;
                }
                self.hotlines = hotlines;
                $rootScope.hotlines = hotlines;
                checkRoute();
                isAutorisedToCreate();
            }

            function getHotlinesAPI(){
                if($rootScope.isAdministrator){
                    Hotlines.getAllHotlines($rootScope.currentUser.userName, callbackHotlineRequest)
                }
                else{
                    Hotlines.getUserHotlines($rootScope.currentUser.userName, callbackHotlineRequest);
                }
            }

            var isAutorisedToCreate = function () {
                // an administrator will not create hotline, he had to complete them !
                if($rootScope.isAdministrator){
                    self.autorisedCreation = false;
                    return;
                }
                self.autorisedCreation = true;
                angular.forEach(self.hotlines, function(value,key){
                    if(value.State !== 2)
                    {
                        // if not the totallity of the user's hotline has noot been completed
                        // he can't create more hotlines
                        self.autorisedCreation = false;
                        return;
                    }
                });
            };

            this.getName = function(mail){
                return Hotlines.getName(mail);
            };

            this.deleteHotline = function (id) {
                Hotlines.deleteHotline($rootScope.currentUser.userName, id, callbackDeleteHotline);
            };

            function callbackDeleteHotline(id, success){
                if(success){
                    $rootScope.hotlineRemoved = id;
                    $location.path('/hotlines/success/'+id);
                    getHotlinesAPI();
                }
                else{
                    $location.path('/hotlines/error/'+id);
                }
            }

            function successDefinition(){
                self.successMessage += "Opération sur la hotline : succès !\n";
                self.showSuccess = true;
            };

            function failedDefinition(){
                self.alertMessage += "Opération sur la hotline : erreur !\n";
                self.showAlert = true;
            };

            function errorLoading() {
                self.alertMessage += "Impossible d'obtenir les hotlines ! \n";
                self.showAlert = true;
            };

            /************************** Start Main **********************************************/

            if($window.sessionStorage.getItem("adal.token.keys") !== undefined
                && $window.sessionStorage.getItem("adal.token.keys") !== null
                && $window.sessionStorage.getItem("adal.token.keys") !== ""){
                // We need to remove the last character of adal.token.keys
                // There is a '|' added at the end
                validateToken(false,
                    $window.sessionStorage.getItem("adal.access.token.key"+
                        $window.sessionStorage.getItem("adal.token.keys").substring(0,$window.sessionStorage.getItem("adal.token.keys").length-1))
                );
            }
            /*else{
                getHotlinesAPI();
            }*/

            /*********************** End of Main ********************************************/

        }]
    });