'use strict';

angular.module('core.members')
    .factory('MembersList',['$resource',function ($resource) {
        return $resource('resources/json/members.json',{},{
            query : {
                method : 'GET',
                isArray : true
            }
        })
    }]);