'use strict';

angular.module('core.userStatus')
    .factory('UserStatus',['ApiToken','$http','$rootScope',function (ApiToken, $http,$rootScope) {
        var userApiUrl = "http://localhost:18303/api/hotlines";

        function sendRequest(access_token, callback, mail) {
            $http({
                method: 'GET',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type' : 'application/json',
                    'Cache-Control' : 'no-cache',
                    'Authorization' : 'Bearer ' + access_token
                },
                url: userApiUrl + mail
            }).success(function (response) {
                callback(response);
            });
        }
        return {
            isAdministrator : function(mail, callback){
                // if token access is not already set
                // and the token is not expired : we just reuse it
                if( ApiToken.checkTokenValidity() ){
                    sendRequest($rootScope.tokenAccess.value, callback, mail);
                    return;
                }

                ApiToken.getAccessToken().success(function (response) {
                    // invallid client_id (not set or invalid)
                    if(response.hasOwnProperty('invalid_clientId')) return null;

                    ApiToken.storeAccessToken(response.access_token, response.expires_in);
                    sendRequest(response.access_token, callback, mail);
                }).error(function (error) {
                    console.log(error);
                    callback(null);
                });
            }

        }
    }]);