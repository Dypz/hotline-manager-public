'use strict';

angular.module('core.hotlines')
    .factory('Hotlines',['ApiToken','$http','$rootScope',function (ApiToken, $http, $rootScope) {
        var hotlinesApiUrl = "http://localhost:18303/api/hotlines";

        function requestAllHotlines(mail, access_token, callback) {
            $http({
                method: 'GET',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type' : 'application/json',
                    'Cache-Control' : 'no-cache',
                    'Authorization' : 'Bearer ' + access_token
                },
                url: hotlinesApiUrl +"/admin?mail=" + mail
            }).success(function (response) {
                callback(response);
            }).error(function(error){
                console.log(error);
                callback(null);
            });
        }

        function requestUserHotlines(mail, access_token, callback) {
            $http({
                method: 'GET',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type' : 'application/json',
                    'Cache-Control' : 'no-cache',
                    'Authorization' : 'Bearer ' + access_token
                },
                url: hotlinesApiUrl +"?mail=" + mail
            }).success(function (response) {
                callback(response);
            }).error(function(error){
                console.log(error);
                callback(null);
            });
        }

        function requestDeleteHotline(mail, id, access_token, callback) {
            $http({
                method: 'DELETE',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type' : 'application/json',
                    'Cache-Control' : 'no-cache',
                    'Authorization' : 'Bearer ' + access_token
                },
                url: hotlinesApiUrl + "/delete?id=" + id + "&mail=" + mail
            }).success(function (response) {
                callback(id,response);
            }).error(function(error){
                console.log(error);
                callback(id, null);
            });
        }

        function requestCreateHotline(hotline, access_token, callback) {
            $http({
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type' : 'application/json',
                    'Cache-Control' : 'no-cache',
                    'Authorization' : 'Bearer ' + access_token
                },
                url: hotlinesApiUrl + "/create",
                data : angular.toJson(hotline)
            }).success(function (response) {
                callback(hotline.HotlineID,response);
            }).error(function(error){
                console.log("error" + error);
                callback(hotline.HotlineID, error);
            });
        }

        function requestUpdateHotline(hotline, access_token, callback){
            $http({
                method: 'PUT',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type' : 'application/json',
                    'Cache-Control' : 'no-cache',
                    'Authorization' : 'Bearer ' + access_token
                },
                url: hotlinesApiUrl + "/update",
                data : angular.toJson(hotline)
            }).success(function (response) {
                callback(hotline.HotlineID,response);
            }).error(function(error){
                callback(hotline.HotlineID, error);
            });
        }
        return {
            // get all hotlines of the user
            getAllHotlines: function (mail, callback) {
                // if token access is not already existing
                // and the token is not expired : we just reuse it
                if( ApiToken.checkTokenValidity() ){
                    requestAllHotlines(mail, $rootScope.tokenAccess.value, callback);
                    return;
                }
                ApiToken.getAccessToken().success(function (response) {
                    // invallid client_id (not set or invalid)
                    if(response.hasOwnProperty('invalid_clientId')) return null;

                    ApiToken.storeAccessToken(response.access_token, response.expires_in);
                    requestAllHotlines(mail, response.access_token, callback);
                }).error(function (error) {
                    console.log(error);
                    callback(null);
                });
            },

            // get hotlines of a precise user using his mail
            getUserHotlines : function (mail, callback) {
                // if token access is already existing
                // and the token is not expired : we just reuse it
                if( ApiToken.checkTokenValidity() ){
                    requestUserHotlines(mail, $rootScope.tokenAccess.value, callback);
                    return;
                }

                ApiToken.getAccessToken().success(function (response) {
                    // invallid client_id (not set or invalid)
                    if(response.hasOwnProperty('invalid_clientId')) return null;

                    ApiToken.storeAccessToken(response.access_token, response.expires_in);
                    requestUserHotlines(mail, response.access_token, callback);
                }).error(function (error) {
                    console.log(error);
                    callback(null);
                });
            },

            deleteHotline : function (mail, id, callback) {
                // if token access is already existing
                // and the token is not expired : we just reuse it
                if( ApiToken.checkTokenValidity() ){
                    requestDeleteHotline(mail, id, $rootScope.tokenAccess.value, callback);
                    return;
                }

                ApiToken.getAccessToken().success(function (response) {
                    // invallid client_id (not set or invalid)
                    if(response.hasOwnProperty('invalid_clientId')) return null;

                    ApiToken.storeAccessToken(response.access_token, response.expires_in);
                    requestDeleteHotline(mail,id, response.access_token, callback);
                }).error(function (error) {
                    console.log(error);
                    callback(null, null);
                });
            },

            createHotline : function (hotline, callback) {
                // if token access is already existing
                // and the token is not expired : we just reuse it
                console.log(hotline);
                if( ApiToken.checkTokenValidity() ){
                    console.log($rootScope.tokenAccess.value);
                    requestCreateHotline(hotline, $rootScope.tokenAccess.value, callback);
                    return;
                }

                ApiToken.getAccessToken().success(function (response) {
                    // invallid client_id (not set or invalid)
                    if(response.hasOwnProperty('invalid_clientId')) return null;

                    ApiToken.storeAccessToken(response.access_token, response.expires_in);
                    requestCreateHotline(hotline, response.access_token, callback);
                }).error(function (error) {
                    console.log(error);
                    callback(null, false);
                });
            },
            
            updateHotline : function (hotline, callback) {
                // if token access is not already existing
                // and the token is not expired : we just reuse it
                if( ApiToken.checkTokenValidity() ){
                    requestUpdateHotline(hotline, $rootScope.tokenAccess.value, callback);
                    return;
                }

                ApiToken.getAccessToken().success(function (response) {
                    // invallid client_id (not set or invalid)
                    if(response.hasOwnProperty('invalid_clientId')) return null;

                    ApiToken.storeAccessToken(response.access_token, response.expires_in);
                    requestUpdateHotline(hotline, response.access_token, callback);
                }).error(function (error) {
                    console.log(error);
                    callback(null, false);
                });
            },

            getName : function (mail) {
                if(mail === null || mail === undefined || mail === "")
                    return "Nobody";
                // Mail format is firstname.lastname@isen.yncrea.fr
                var mailSplit = mail.split('.');
                // Set only the first letter of the firstname to upper case
                var firstName = mailSplit[0].charAt(0).toUpperCase() + mailSplit[0].slice(1);
                var lastName = (mailSplit[1].split('@'))[0].toUpperCase();
                return firstName + " " + lastName;
            },

            getDateTime : function (input) {
                try {
                    // input respect the following form : yyyy-mm-dd hh:mm
                    var myDate = input.split("-");
                    var year = myDate[0];
                    var month = myDate[1];
                    if(month != '03'){
                        return null;
                    }
                    var day = (myDate[2].split(" "))[0];

                    if(day < 27 || day > 31){
                        return null;
                    }

                    var time = input.split(":");
                    var hour = (time[0].split(" "))[1];

                    if(hour < 7 || hour > 21){
                        return null;
                    }
                    var minute = time[1];
                    // -1 on month because month are counted from 0 to 11
                    // +1 on hour otherwise we lost one hour in database
                    /*return (new Date(parseInt(year, 10), parseInt(month, 10) - 1, parseInt(day, 10),
                        parseInt(hour, 10) + 1, parseInt(minute, 10))).toDatetime();*/
                    return input;
                }
                catch (err) {
                    return null;
                }
            }

        };
    }]);