'use strict';

angular.module('core.apiToken')
    .factory('ApiToken',['$http','$rootScope',function ($http, $rootScope) {
        var apiUrl = "http://localhost:18292/oauth2/token";
        return {
            // get the access token to interact with the hotline API
            getAccessToken: function () {
                // TODO : First we need to register the audience normally
                // TODO : Will do it later when the client_id wil not be hardcoded
                var req = {
                    method: 'POST',
                    url: apiUrl,
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded',
                        'Accept' : 'application/json'
                    },
                    data : 'username='+$rootScope.currentUser.userName+'&password=SysAdmin&grant_type=password&client_id=099153c2625149bc8ecb3e85e03f0022'
                };
                return $http(req);
            },

            checkTokenValidity : function () {
                if($rootScope.tokenAccess.value != null && $rootScope.tokenAccess.expiration_time != null &&
                    $rootScope.tokenAccess.expiration_time > new Date() )
                    return true;
                return false;
            },

            storeAccessToken : function(access_token, expires_in){
                $rootScope.tokenAccess.value = access_token;
                // Expiration time = now + token time validity
                $rootScope.tokenAccess.expiration_time = moment(new Date()).add(expires_in,'seconds');
            }

        };
    }]);