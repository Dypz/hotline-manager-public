'use strict';

angular.module('editHotline')
    .component('editHotline',{
        templateUrl : '/edit-hotline/edit-hotline.template.html',
        controller : ['$scope','$routeParams','$rootScope','$location','Hotlines','randomString',
            function($scope,$routeParams,$rootScope,$location,Hotlines,randomString){

            if($rootScope.currentUser == null || $rootScope.isAdministrator)
            {
                $location.path('/hotlines');
                return;
            }

            var self = this;

            this.errorOnInputs = false;
            // Accepted beginning formats for phone number :  +33x, 06-07
            $scope.phoneRegex = "^((0[6-7]{1})|\\+33[6-7])(([0-9]{2}){4})|((\s[0-9]{2}){4})|((-[0-9]{2}){4})$";

            this.hotline = {
                    Address : null,
                    Date : null,
                    Description : null,
                    State : '0',
                    PhoneNumber : null,
                    AuthorMail : null,
                    HotlineResponsibleMail : null,
                    HotlineID : randomString(254)
                };

            var initInput = function(){
                    $scope.address = self.hotline.Address;
                    $scope.description = self.hotline.Description;
                    $scope.phoneNumber = self.hotline.PhoneNumber;
                    if(moment(self.hotline.Date).isValid())
                        $scope.hotlineDate = moment(self.hotline.Date).format("YYYY-MM-DD HH:mm");
                    else
                        $scope.hotlineDate = moment(); // return current date
                };

            function getHotlinesAPI(){
                Hotlines.getUserHotlines($rootScope.currentUser.userName).success(function (response) {
                    $rootScope.hotlines = response.GetUserHotlineListResult;
                    initHotline();
                }).error(function(){
                    $location.path('/hotlines');
                });
            };

            var initHotline = function(){
                if($routeParams.hotlineId !== undefined){
                    angular.forEach($rootScope.hotlines,function (value, key) {
                        if(value.HotlineID == $routeParams.hotlineId){
                            self.hotline.Address = value.Address;
                            self.hotline.Date = value.Date;
                            self.hotline.Description = value.Description;
                            self.hotline.State = value.State;
                            self.hotline.PhoneNumber = value.PhoneNumber;
                            self.hotline.AuthorMail = value.AuthorMail;
                            self.hotline.HotlineResponsibleMail = value.HotlineResponsibleMail;
                            self.hotline.HotlineID = value.HotlineID;
                            initInput();
                        }
                    });
                }
            };

            var anyEmptyInput = function(){
                if($scope.hotlineForm.address.$error.required ||
                    $scope.hotlineForm.description.$error.required ||
                    $scope.hotlineForm.date.$error.required ) {
                    setErrorView("Tous les champs sont requis !");
                    return true;
                }
                return false;
            };

            var inputsLength = function () {
                if($scope.hotlineForm.description.$error.maxlength){
                    setErrorView("Description trop courte !");
                    return true;
                }
                if($scope.hotlineForm.address.$error.maxlength){
                    setErrorView("Adresse trop courte !");
                    return true;
                }
                if($scope.hotlineForm.address.$error.minlength){
                    setErrorView("Adresse trop courte !");
                    return true;
                }
                return false;
            };

            var checkPhoneFormat = function(){
                if($scope.hotlineForm.phone.$error.pattern){
                    setErrorView("Le format du numéro de téléphone est incorrect !");
                    return true;
                }
                return false;
            };

            var setErrorView = function(msg){
                self.errorOnInputs = true;
                self.alertInputs = msg;
            };

            this.sendHotline = function () {
                // Inputs must not be empty
                if(anyEmptyInput()) return;
                // Specific inputs must respect a minimum and maximum length
                if(inputsLength()) return;
                // Check the phone number format
                if(checkPhoneFormat()) return;

                var date = Hotlines.getDateTime($scope.hotlineDate);
                // If date is null, the input date is not correct so we no longer continue
                if(date === null)
                {
                    setErrorView("Les hotlines ne sont disponibles que du Lundi 27 au Vendredi 31 (7h-21h); hors de ces horaires vous ne pouvez pas en créer. Sinon le format de la date est incorrect");
                    return;
                }

                //self.hotline.Date = date;
                self.hotline.Date = $scope.hotlineDate;
                self.hotline.Address = $scope.address;
                self.hotline.Description = $scope.description;
                self.hotline.PhoneNumber = $scope.phoneNumber;
                self.hotline.AuthorMail = $rootScope.currentUser.userName;

                if($routeParams.hotlineId !== undefined){
                    Hotlines.updateHotline(self.hotline, callbackHotline);
                }
                else{
                    Hotlines.createHotline(self.hotline, callbackHotline)
                }
            };

            function callbackHotline(hotlineID, success){
                if(success){
                    $location.path('/hotlines/success/'+hotlineID);
                }
                else{
                    console.log(success);
                    $location.path('/hotlines/error/'+hotlineID);
                }

            }

            /******************************** Start Main **********************************/

            // It could happen when a user refresh the page, indeed we load hotlines in hotline manager only normally
            if($rootScope.hotlines === "")
            {
                getHotlinesAPI();
                initHotline();
            }
            else{
                initHotline();
            }

            /****************************** End of Main *********************************/


        }]

    });