'use strict';

angular.module('members')
    .component('members',{
        templateUrl : 'members/members.template.html',
        controller : ['MembersList', function(MembersList){
            this.members = MembersList.query();
        }]
    });
