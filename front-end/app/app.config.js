'use strict';

angular.module('hotlineManagerApp')
    .config(['$locationProvider','$routeProvider',
        function($locationProvider,$routeProvider){
            $locationProvider.hashPrefix('!');

            $routeProvider
                .when('/home',{
                    template : '<home></home>'
                })
                .when('/campaign',{
                template : '<campaign></campaign>'
                })
                .when('/hotlines/:status?/:hotlineId?',{
                    template : '<hotline-manager></hotline-manager>'
                })
                .when('/edit/:hotlineId?',{
                    template : '<edit-hotline></edit-hotline>'
                })
                .when('/editAdmin/:hotlineId',{
                    template : '<edit-hotline-administrator></edit-hotline-administrator>'
                })
                .when('/members',{
                    template : '<members></members>'
                })
                .otherwise('/home');
        }])
        .run(function ($rootScope) {
            $rootScope.hotlines = "";
            $rootScope.isAdministrator = false;
            $rootScope.hotlineRemoved = "";
            $rootScope.currentUser = null;
            $rootScope.tokenAccess = {
                value : null,
                expiration_time : null
            };

            // define a prototype on date to convert it to C# Datetime
            Date.prototype.toDatetime = function () {
                var date = '/Date(' + this.getTime() + ')/';
                return date;
            };
        });
