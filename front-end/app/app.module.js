'use strict';

angular.module('hotlineManagerApp',[
    'ngRoute',
    'ngAnimate',
    'campaign',
    'hotlineManager',
    'home',
    'editHotline',
    'editHotlineAdministrator',
    'members'
]);