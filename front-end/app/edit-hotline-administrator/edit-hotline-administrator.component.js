'use strict';

angular.module('editHotlineAdministrator')
    .component('editHotlineAdministrator',{
        templateUrl : 'edit-hotline-administrator/edit-hotline-administrator.template.html',
        controller : ['Hotlines','$routeParams','$rootScope','$location',
            function(Hotlines,$routeParams,$rootScope,$location){

            if($rootScope.currentUser == null || !$rootScope.isAdministrator){
                $location.path('/hotlines');
                return;
            }

            var self = this;

            this.hotline = {
                Address : null,
                Date : null,
                Description : null,
                State : null,
                PhoneNumber : null,
                AuthorMail : null,
                HotlineResponsibleMail : null,
                HotlineID : null
            };

            function getHotlinesAPI(){
                Hotlines.getAllHotlines().success(function (response) {
                    $rootScope.hotlines = response.GetHotlineListResult;
                    if(!checkHotlineId())
                    // redirect to the home if hotlineId is not defined in the URL
                    // no one can access to this page with an incorrect hotline id
                        $location.path('/home');
                    defineAdminAction();
                }).error(function(){
                    $location.path('/hotlines');
                });
            }

            function checkHotlineId(){
                if($routeParams.hotlineId !== undefined){
                    var validId = false;
                    angular.forEach($rootScope.hotlines,function (value, key) {
                        if(value.HotlineID == $routeParams.hotlineId){
                            self.hotline.Address = value.Address;
                            self.hotline.Date = moment(value.Date).format("YYYY-MM-DD HH:mm");
                            self.hotline.Description = value.Description;
                            self.hotline.State = value.State;
                            self.hotline.PhoneNumber = value.PhoneNumber;
                            self.hotline.AuthorMail = value.AuthorMail;
                            self.hotline.HotlineResponsibleMail = value.HotlineResponsibleMail;
                            self.hotline.HotlineID = value.HotlineID;
                            validId = true;
                            return;
                        }
                    });
                    return validId;
                }
                else{
                    return false;
                }
            }

            function defineAdminAction(){
                if(self.hotline.State === 0){
                    self.changeState = "Prendre en charge la hotline avec le sourire !";
                } else if (self.hotline.State === 1){
                    self.changeState = "Hotline accomplie, on ferme ! ";
                }
            }

            this.updateState = function(){
                self.hotline.State += 1;
                self.hotline.Date = Hotlines.getDateTime(self.hotline.Date);
                self.hotline.HotlineResponsibleMail = $rootScope.currentUser.userName;
                Hotlines.updateHotline(self.hotline, callbackHotline);
            };

            function callbackHotline(hotlineID, success){
                if(success){
                    $location.path('/hotlines/success/'+hotlineID);
                }
                else{
                    $location.path('/hotlines/error/'+hotlineID);
                }
            }

            this.getName = function(mail){
                return Hotlines.getName(mail);
            };

            /******************************* Start main *****************************/
            // It could happen when a user refresh the page
            // So we just reload the hotlines according to the current user
            if($rootScope.hotlines === ""){
                // The checking id is done in this function
                getHotlinesAPI();
            }
            else{
                // Normal case : we just check the id of the hotline
                if(!checkHotlineId())
                // Redirect to the home if hotlineId is not defined in the URL
                // No one can access to this page with an incorrect hotline id
                    $location.path('/home');
                defineAdminAction();
            }

            /********************************* End of main *********************************/
        }]
    });